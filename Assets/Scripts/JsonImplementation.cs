using Butter.Database.Json;
using Butter;
using Butter.Database;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

[JsonTemplate]
public struct ReleaseFlag : IModel {
	public bool isExperimental;
	public bool isRelease;
	public string Name { get; set; }

	public override string ToString() {
		return $"isExp = {isExperimental} | isRelease = {isRelease}";
	}
	public static implicit operator bool(ReleaseFlag flag) => flag.isExperimental;
}

public class ReleaseFlagFilter : FilterByName<ReleaseFlag> {

	public enum FlagType {
		None,
		Experimental,
		Release
	}

	public FlagType desiredFlagType;

	public override List<ReleaseFlag> Result(List<ReleaseFlag> input) {
		var query = base.Result(input);
		switch (desiredFlagType) {
			case FlagType.Experimental:
				return query.Where(flag => flag.isExperimental).ToList();
			case FlagType.Release:
				return query.Where(flag => flag.isRelease).ToList();
		}
		return query;
	}
}

public partial class ReleaseFlagAdmin : JsonAdmin<ReleaseFlag> {
	public override IFilter<ReleaseFlag> InitFilter() {
		return new ReleaseFlagFilter();
	}
}

#if UNITY_EDITOR
public partial class ReleaseFlagWindow : AdminWindow<ReleaseFlag, ReleaseFlagAdmin> {
	[MenuItem("Database/ReleaseFlagWindow")]
	public static void OpenWindow() {
		GetWindow<ReleaseFlagWindow>().Show();
	}
}
#endif