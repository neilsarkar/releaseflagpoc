﻿using Butter;
using Butter.Database.UnityAddressables;
using UnityEngine;

[AddressableTemplate]
public class FooBlueprint : ScriptableObject, IModel {
	public bool isHairy;
	public int numLegs;
	public string lastName;

	public string Name {
		get => this.name;
		set => this.name = value;
	}
}