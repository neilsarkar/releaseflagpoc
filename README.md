
# Butter

Butter is a C# library for generating databases from game content objects. Currently, this library is made to be used in conjunction with unity.

## Dependencies

### Odin Serializer

https://odininspector.com/

### UniTask

https://github.com/Cysharp/UniTask

    openupm add com.cysharp.unitask

### Addressables

<img src="https://media.discordapp.net/attachments/559829875238633502/834152792880054342/SampleScene_-_butterclient_-_PC__Mac___Linux_Standalone_-_Unity_2019_4_15f1_Personal__Personal___Met.png" />

### Newtonsoft Json

Add this to `Packages/manifest.json`

    "com.unity.nuget.newtonsoft-json": "2.0.1-preview.1",

## Installation

Copy the Butter folder into any unity project.

    cp -r /path/to/releaseflagpoc/Assets/Butter /path/to/project/Assets/Butter

## Basic Usage

Define your data in a class or struct:

```csharp
// Creature.cs

using Butter;
using Butter.Database.Json;

[JsonTemplate]
public struct Creature : IModel {

    public int numLegs;
    public bool isHairy;
    public string Name {get; set;}

}

```

Generate code for all the templates using the database generator. (`Cmd+Shift+G`/`Ctrl+Shift+G` in Unity):

```csharp
Butter.DatabaseGenerator.GenerateDatabases();
```

Create an admin window to manage your data:

```csharp
// CreatureWindow.cs

using Butter.Database;
using UnityEditor;

public class CreatureWindow : AdminWindow<Creature, CreatureAdmin> {
    [MenuItem("MyProject/Creatures")]
    public static void OpenWindow() {
        GetWindow<CreatureWindow>().Show();
    }
}
```

## Unity Usage

To store and query data that references unity objects (Prefabs, Sprites, GameObjects etc) we have to use the ScriptableObject backend.

```csharp
// Player.cs

using Butter;
using Butter.Database.UnityAddressables;

[AddressableTemplate]
public class Player : ScriptableObject, IModel {

    public Sprite sprite;
    public List<GameObject> prefabs;
    public string Name {
        get => this.name;
        set => this.name = value;
    }
}
```

```csharp
// PlayerWindow.cs

using Butter.Database;
using UnityEditor;

public class PlayerWindow : AdminWindow<Player, PlayerAdmin> {
    [MenuItem("MyProject/Players")]
    public static void OpenWindow() {
        GetWindow<PlayerWindow>().Show();
    }
}
```

## Querying

```csharp
// Get all
List<Player> players = await PlayerQueries.All();

// Get one by id
Player player = await PlayerQueries.Get(PlayerId.Mario);

// Update by id
player.sprite = null;
await PlayerQueries.Update(PlayerId.Mario, player);

// Delete by id
await PlayerQueries.Delete(PlayerId.PlayerId.Mario)
```


## Custom Templates
By inheriting from the template class, it is possible to run any code during the database generation function.

```csharp
// MyCustomTemplate.cs

using Butter;

public class MyCustomTemplate : Template {
	public override void GenerateDatabases() {

		//run any code here
        print ($"{modelName}");

	}
}

// Creature.cs

[CustomTemplate]
public struct Creature : IModel {

    public int numLegs;
    public bool isHairy;
    public string Name {get; set;}

}

```