﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Butter.Database.UnityAddressables {
	public abstract class AddressableAdmin<TModel> : Admin<TModel> where TModel : ScriptableObject, IModel {
		public override IQueryable<TModel> Queries => crud;
		public override IFactory<TModel> Factory => crud;
		[HideInInspector]
		public AddressableCrud<TModel> crud;
		public AddressableAdmin() {
			crud = new AddressableCrud<TModel>();
		}
	}
}