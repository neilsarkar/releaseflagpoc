﻿using System;
using System.IO;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets;

namespace Butter.Database.UnityAddressables.Editor {
	public class AddressableWatcher<TModel> where TModel : UnityEngine.ScriptableObject, IModel {
		public readonly static string path = Path.GetFullPath("Assets/");
		public const string fileExt = ".asset";
		private FileSystemWatcher watcher;
		private event Action UpdateCreated;
		private event Action UpdateChanged;
		private event Action UpdateDeleted;

		public void Watch() {
			watcher = new FileSystemWatcher(path);
			watcher.Filter = "*" + fileExt;
			watcher.IncludeSubdirectories = true;
			watcher.Created += OnCreated;
			watcher.Deleted += OnDeleted;
			watcher.Renamed += OnChanged;
			watcher.Changed += OnChanged;
			watcher.EnableRaisingEvents = true;

			EditorApplication.update -= Update;
			EditorApplication.update += Update;
		}

		private void OnCreated(object source, FileSystemEventArgs e) {
			var assetPath = e.FullPath.Split(new string[] { "Assets/" }, System.StringSplitOptions.RemoveEmptyEntries)[1];
			assetPath = "Assets/" + assetPath;
			UpdateCreated += AddressableCreated;
			void AddressableCreated() {
				UpdateCreated -= AddressableCreated;
				var label = typeof(TModel).Name;
				var assets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
				foreach (var asset in assets) {
					if (asset.GetType() == typeof(TModel)) {
						TModel tAsset = (TModel)asset;
						AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
						var group = settings.FindGroup(label);
						if (group == null) {
							group = settings.CreateGroup(label, false, false, true, settings.DefaultGroup.Schemas);
							settings.SetDirty(AddressableAssetSettings.ModificationEvent.GroupAdded, group, true);
						}
						settings.AddLabel(label, true);
						var guid = AssetDatabase.AssetPathToGUID(assetPath);
						var entry = settings.CreateOrMoveEntry(guid, group);
						entry.address = $"Database/{label}/{tAsset.name}";
						entry.labels.Add(label);
						settings.CreateOrMoveEntry(entry.guid, group);
						settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryCreated, entry, true);
						settings.SetDirty(AddressableAssetSettings.ModificationEvent.LabelAdded, label, true);
						AssetDatabase.SaveAssets();
						_ = AddressablesHelpers.RefreshAddressables();
					}
				}
			}
		}

		private void OnChanged(object source, FileSystemEventArgs e) {
			var assetPath = e.FullPath.Split(new string[] { "Assets/" }, System.StringSplitOptions.RemoveEmptyEntries)[1];
			assetPath = "Assets/" + assetPath;
			UpdateCreated += AddressableChanged;
			void AddressableChanged() {
				UpdateCreated -= AddressableChanged;
				var label = typeof(TModel).Name;
				var assets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
				foreach (var asset in assets) {
					if (asset.GetType() == typeof(TModel)) {
						TModel tAsset = (TModel)asset;
						AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
						var guid = AssetDatabase.AssetPathToGUID(assetPath);
						var entry = settings.FindAssetEntry(guid);
						var targetAddress = $"Database/{label}/{tAsset.name}";
						if (entry.address != targetAddress) {
							entry.address = targetAddress;
							settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryModified, entry, true);
							AssetDatabase.SaveAssets();
							_ = AddressablesHelpers.RefreshAddressables();
						}
					}
				}
			}
		}

		private void OnDeleted(object source, FileSystemEventArgs e) {
			UnityEngine.Debug.Log($"Deleted {e.FullPath}");
			var assetPath = e.FullPath.Split(new string[] { "Assets/" }, System.StringSplitOptions.RemoveEmptyEntries)[1];
			assetPath = "Assets/" + assetPath;
			UpdateDeleted += DeleteAddressable;

			void DeleteAddressable() {
				UpdateDeleted -= DeleteAddressable;
				_ = AddressablesHelpers.RefreshAddressables();
			}
		}

		private void Update() {
			UpdateCreated?.Invoke();
			UpdateChanged?.Invoke();
			UpdateDeleted?.Invoke();
		}
	}
}
