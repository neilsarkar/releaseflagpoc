﻿using System.Threading;
using Cysharp.Threading.Tasks;

namespace Butter.Database {
	public interface IFactory<TModel> where TModel : IModel {
		UniTask<TModel> Create(string name, CancellationToken cancellationToken);
	}
}