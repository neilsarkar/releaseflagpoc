﻿using System.Collections.Generic;

namespace Butter.Database {
	public interface IFilter<TModel> where TModel : IModel {
		List<TModel> Result(List<TModel> input);
	}
}