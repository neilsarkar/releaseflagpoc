﻿namespace Butter.Database {
	public abstract class DatabaseTemplate : Template {
		protected virtual string modelQueryName => $"{modelName}Queries";
		protected virtual string databaseDir => "Assets/Database";
		protected virtual string dataPath => $"{databaseDir}/{modelName}";
	}

}