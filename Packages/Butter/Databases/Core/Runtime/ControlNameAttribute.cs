﻿using UnityEngine;

public class ControlNameAttribute : PropertyAttribute {
	public string controlName;
	public ControlNameAttribute(string controlName) {
		this.controlName = controlName;
	}
}