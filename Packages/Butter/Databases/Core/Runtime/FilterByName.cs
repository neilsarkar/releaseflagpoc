﻿using System.Collections.Generic;
using System.Linq;

namespace Butter.Database {
	public class FilterByName<TModel> : IFilter<TModel> where TModel : IModel {
		public string desiredName;
		public virtual List<TModel> Result(List<TModel> input) {
			if (string.IsNullOrEmpty(desiredName)) {
				return input;
			}
			return input.Where(item => item.Name.ToLower().Contains(desiredName.ToLower())).ToList();
		}
	}
}