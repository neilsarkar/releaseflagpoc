﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Butter.Database {
	[System.Serializable]
	[InlineProperty]
	[HideLabel]
	public abstract class Admin<TModel> where TModel : IModel {
		public const string ControlName = "NameCreationField";
		public virtual CancellationTokenSource TokenSource { get; set; }
		public abstract IQueryable<TModel> Queries { get; }
		public abstract IFactory<TModel> Factory { get; }

		[ShowInInspector]
		[InlineProperty]
		[HideLabel]
		[Title("Filter")]
		public IFilter<TModel> Filter {
			get => filter = (filter is null) ? InitFilter() : filter;
			protected set => filter = value;
		}
		private IFilter<TModel> filter;

		[Title("Create New")]
		[HideLabel]
		[InlineButton("CreateOdin", "Create (Enter)")]
		[ControlName(ControlName)]
		public string newInstanceName;
		string sanitizedName => Regex.Replace(Regex.Replace(newInstanceName, "[^A-z0-9]", ""), "^[0-9]+", "");

		public Admin() {
			InitFilter();
			TokenSource = new CancellationTokenSource();
			models = new List<TModel>();
			filteredModels = new List<TModel>();
		}

		[ShowInInspector]
		[Title("Edit Values")]
		[ListDrawerSettings(
			HideAddButton = true,
			Expanded = true,
			AlwaysAddDefaultValue = true,
			CustomRemoveElementFunction = "DeleteOdin"
			)]
		public List<TModel> Models {
			get {
				filteredModels = filter.Result(models);
				return filteredModels;
			}
			set => models = value;
		}

		protected List<TModel> filteredModels;
		protected List<TModel> models;

		public async virtual UniTask Setup() {
			models = await Queries.All(TokenSource.Token);
		}

		public async virtual UniTask TearDown() {
			TokenSource.Cancel();
		}

		public virtual IFilter<TModel> InitFilter() {
			return new FilterByName<TModel>();
		}

		//need to use void here or it wont work with odin list
		private async UniTaskVoid CreateOdin() {
			await Create(TokenSource.Token);
		}

		protected virtual async UniTask Create(CancellationToken cancellationToken) {
			await Factory.Create(sanitizedName, cancellationToken);
		}
		//need to use void here or it wont work with odin list
		protected void DeleteOdin(TModel model) {
			_ = Delete(model, TokenSource.Token);
		}
		protected virtual async UniTask Delete(TModel model, CancellationToken cancellationToken) {
			await Queries.Delete(model.Name, cancellationToken);
			await UniTask.DelayFrame(1);
			await UpdateView();
		}

		public virtual async UniTask UpdateView() {
			models = await Queries.All(TokenSource.Token);
		}

		public async virtual UniTask OnInspectorDirty() { }

		public async UniTask ListenForKeyboard() {
			var selectedControlName = GUI.GetNameOfFocusedControl();
			bool controlIsSelected = ControlName == selectedControlName;
			if (!controlIsSelected) { return; }
			if (Event.current.keyCode == KeyCode.Return &&
			Event.current.type == EventType.KeyUp) {
				var name = sanitizedName;
				var all = await Queries.All(TokenSource.Token);
				if (!all?.Exists(x => x.Name == name) ?? true) {
					await Create(TokenSource.Token);
				}
				UnityEngine.Debug.Log($"calling create");
			}
		}
	}
}