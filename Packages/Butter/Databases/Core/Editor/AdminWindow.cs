﻿using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;

namespace Butter.Database {
	public abstract class AdminWindow<TModel, TAdmin> : OdinEditorWindow
		where TModel : IModel
		where TAdmin : Admin<TModel>, new() {
		public Admin<TModel> admin;
		protected new async virtual UniTask OnEnable() {
			base.OnEnable();
			admin = new TAdmin();
			await admin.Setup();
		}
		protected new async virtual UniTask OnDestroy() {
			base.OnDestroy();
			await admin.TearDown();
		}
		bool isDirty = false;
		void OnValidate() {
			isDirty = true;
		}

		public async UniTaskVoid Update() {
			if (isDirty) {
				await admin.OnInspectorDirty();
				isDirty = false;
			} else {
				await admin.UpdateView();
			}
		}

		protected override void OnGUI() {
			base.OnGUI();
			_ = admin.ListenForKeyboard();
		}
	}
}