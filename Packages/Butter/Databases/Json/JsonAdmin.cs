﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Butter.Database.Json {
	public abstract class JsonAdmin<TModel> : Admin<TModel> where TModel : IModel, new() {
		public override IQueryable<TModel> Queries => crud;
		public override IFactory<TModel> Factory => crud;
		[HideInInspector]
		public JsonCrud<TModel> crud;
		public JsonAdmin() {
			crud = new JsonCrud<TModel>();
		}
		public async override UniTask OnInspectorDirty() {
			await base.OnInspectorDirty();
			for (int i = 0; i < filteredModels.Count; i++) {
				var model = filteredModels[i];
				int index = System.Array.FindIndex<TModel>(models.ToArray(), m => m.Name == model.Name);//models.IndexOf(model);
				if (index >= 0) {
					models[index] = model;
				} else {
					models.Add(model);
				}

			}
			await crud.ReplaceAll(models, TokenSource.Token);
		}
	}
}