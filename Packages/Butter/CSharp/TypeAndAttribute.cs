﻿using System;
using System.Collections.Generic;

namespace Butter {
	public class TypeAndAttributes<T> where T : Attribute {
		public Type type;
		public IEnumerable<T> attributes;
		public TypeAndAttributes(Type type, IEnumerable<T> attributes) {
			this.type = type;
			this.attributes = attributes;
		}
	}
}