﻿using System.IO;
using System.Reflection;

namespace Butter {
	public abstract class Template : System.Attribute {
		public string modelName { get; protected set; }

		public Template() {
			this.modelName = string.Empty;
		}

		public void SetModelName(string name) {
			modelName = name;
		}

		public abstract void GenerateCode();

	}

}